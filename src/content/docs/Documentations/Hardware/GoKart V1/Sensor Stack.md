---
title: Sensors
description: A reference page in my new Starlight docs site.
---

## Perception sensor
### ZED 2i && Horizon LiDAR
In version 1 of the GoKart, we have deployed only front facing sensor of ZED 2i and Horizon LiDAR
![Zed 2i](../../../../../assets/hardware/GoKartV1/IMG_0857.JPG)


## GPS
### PointOneNav
PointOneNav is a GPS with RTK correction. It is used to provide accurate position and heading information to the GoKart. It is connected to the Jetson Orin via Ethernet. The PointOneNav is mounted on the roof of the GoKart.

![PointOneNav antenna](../../../../../assets/hardware/GoKartV1/IMG_0860.JPG)
![PointOneNav IMU](../../../../../assets/hardware/GoKartV1/IMG_0861.JPG)


# Other sensors
### Speed Sensor
We use an optical speed sensor to measure the speed of the GoKart. The speed sensor is connected to the Arduino via voltage signals. The speed sensor is mounted directly onto the motor shaft.
![Speed Sensor IMU](../../../../../assets/hardware/GoKartV1/IMG_0862.JPG)


### Steering angle sensor
We use a potentiometer to measure the steering angle of the GoKart. The potentiometer is connected to the Arduino via voltage signals. The potentiometer is mounted directly onto the steering column.
![Steering angle sensor IMU](../../../../../assets/hardware/GoKartV1/IMG_0787.JPG)

---
title: Computing
description: The computing and network device we use
---

## Jetson Orin
![Jetson Orin](../../../../../assets/hardware/GoKartV1/IMG_0767.JPG)

## Network
### Switch
![Switch](../../../../../assets/hardware/GoKartV1/IMG_0867.JPG)

### Mobile Router
![Mobile Router](../../../../../assets/hardware/GoKartV1/IMG_0869.JPG)
![Mobile Router](../../../../../assets/hardware/GoKartV1/IMG_0870.JPG)

## Arduino Due
![Arduino Due](../../../../../assets/hardware/GoKartV1/IMG_0853.JPG)
![Arduino Due](../../../../../assets/hardware/GoKartV1/IMG_0774.JPG)


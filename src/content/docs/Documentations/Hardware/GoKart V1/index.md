---
title: Summary
description: Explore the detailed hardware specifications, installation guides, and maintenance instructions of the innovative GoKart V1 on our comprehensive documentation webpage.
---

![](../../../../../assets/hardware/GoKartV1/IMG_0753.JPG)
## Bill of Material
// TODO: @Isabell


## Steering

## Brake

## Throttle

## Computing
See [Computing documentation](/documentations/hardware/gokart-v1/computing/)

## Sensors 
See [Sensors documentation](/documentations/hardware/gokart-v1/sensor-stack/)

## Power Distribution
See [Power Distribution](/documentations/hardware/gokart-v1/power-distribution/)
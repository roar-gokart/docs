---
title: Power Distribution
description: A reference page in my new Starlight docs site.
---

## Battery packs
![48 V battery pack](../../../../../assets/hardware/GoKartV1/IMG_0757.JPG)

### Voltage Monitor
![Voltage Monitor](../../../../../assets/hardware/GoKartV1/IMG_0780.JPG)

## Grounding
![Grounding](../../../../../assets/hardware/GoKartV1/IMG_0865.JPG)

## Fuse and Small voltage system
![Fuse and Small voltage system](../../../../../assets/hardware/GoKartV1/IMG_0866.JPG)




---
title: Controls Infrastructure
description: A guide in my new Starlight docs site.
---

See also

[Control Stack Summary Lark documentation](https://n36411s2sqp.larksuite.com/wiki/R5IPw4sgXiilZKkzyRCuT2v5swb)

[Controls Optimization Lark documentation](https://n36411s2sqp.larksuite.com/wiki/C8Y1wWbcCiQbsVk9g7zuWxL2sog)

[PID Controller Lark documentation](https://n36411s2sqp.larksuite.com/wiki/BMpswcl4giZDoHkyVj3uijUOsOc)

[Steering PID Control Lark documentation](https://n36411s2sqp.larksuite.com/wiki/wikusR2ISZaLsRMAGRJcGpMQgkf)

[Enhance Lateral Controller Lark documentation](https://n36411s2sqp.larksuite.com/wiki/LZeiwzvRXimeNRkUJFquqnyAsUf)

[Manual Control Lark documentation](https://n36411s2sqp.larksuite.com/wiki/IlHhwpgHyidHPEk7DNGuVUvFsMT)

[Local Planner <> Controller Interaction](https://n36411s2sqp.larksuite.com/wiki/wikus2Sqe5GdQ9Re1d8kB5I7lzb)

